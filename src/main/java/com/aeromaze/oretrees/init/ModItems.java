package com.aeromaze.oretrees.init;

import com.aeromaze.oretrees.reference.Reference;
import net.minecraft.item.Item;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(Reference.MOD_ID)
public class ModItems
{

    public static Item.Properties itemProperties()
    {
        return new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP);
    }
}
