package com.aeromaze.oretrees.block.trees;

import com.aeromaze.oretrees.world.gen.feature.OreTreesFeatures;
import net.minecraft.block.trees.Tree;
import net.minecraft.world.biome.DefaultBiomeFeatures;
import net.minecraft.world.gen.feature.BaseTreeFeatureConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Features;

import javax.annotation.Nullable;
import java.util.Random;

public class CoalTree extends Tree
{
    public CoalTree()
    {

    }

    @Nullable
    @Override
    protected ConfiguredFeature<BaseTreeFeatureConfig, ?> getTreeFeature(Random random, boolean b)
    {
        return OreTreesFeatures.COAL;
    }
}
