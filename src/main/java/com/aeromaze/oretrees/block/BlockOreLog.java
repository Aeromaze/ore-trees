package com.aeromaze.oretrees.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.RotatedPillarBlock;

public class BlockOreLog extends RotatedPillarBlock
{
    public BlockOreLog(Properties properties)
    {
        super(properties);
    }
}
