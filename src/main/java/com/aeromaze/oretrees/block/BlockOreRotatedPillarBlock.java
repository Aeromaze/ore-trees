package com.aeromaze.oretrees.block;

import net.minecraft.block.RotatedPillarBlock;

public class BlockOreRotatedPillarBlock extends RotatedPillarBlock
{

    public BlockOreRotatedPillarBlock(Properties properties)
    {
        super(properties);
    }
}
