package com.aeromaze.oretrees.init;

import com.aeromaze.oretrees.reference.Reference;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(Reference.MOD_ID)
public class ModBlocks
{
    public static final Block LOG_COAL = null;
    public static final Block WOOD_COAL = null;
    public static final Block PLANK_COAL = null;
    public static final Block LEAVES_COAL = null;
    public static final Block SAPLING_COAL = null;
    public static final Block LOG_IRON = null;
    public static final Block WOOD_IRON = null;
    public static final Block PLANK_IRON = null;
    public static final Block LEAVES_IRON = null;
    public static final Block SAPLING_IRON = null;
    public static final Block LOG_GOLD = null;
    public static final Block WOOD_GOLD = null;
    public static final Block PLANK_GOLD = null;
    public static final Block LEAVES_GOLD = null;
    public static final Block SAPLING_GOLD = null;
    public static final Block LOG_REDSTONE = null;
    public static final Block WOOD_REDSTONE = null;
    public static final Block PLANK_REDSTONE = null;
    public static final Block LEAVES_REDSTONE = null;
    public static final Block SAPLING_REDSTONE = null;
    public static final Block LOG_LAPIS_LAZULI = null;
    public static final Block WOOD_LAPIS_LAZULI = null;
    public static final Block PLANK_LAPIS_LAZULI = null;
    public static final Block LEAVES_LAPIS_LAZULI = null;
    public static final Block SAPLING_LAPIS_LAZULI = null;

    public static final Block STRIPPED_LOG_COAL = null;
    public static final Block STRIPPED_LOG_IRON = null;
    public static final Block STRIPPED_LOG_GOLD = null;
    public static final Block STRIPPED_LOG_REDSTONE = null;
    public static final Block STRIPPED_LOG_LAPIS_LAZULI = null;
    public static final Block STRIPPED_WOOD_COAL = null;
    public static final Block STRIPPED_WOOD_IRON = null;
    public static final Block STRIPPED_WOOD_GOLD = null;
    public static final Block STRIPPED_WOOD_REDSTONE = null;
    public static final Block STRIPPED_WOOD_LAPIS_LAZULI = null;
    public static final Block STAIRS_COAL = null;
    public static final Block STAIRS_IRON = null;
    public static final Block STAIRS_GOLD = null;
    public static final Block STAIRS_REDSTONE = null;
    public static final Block STAIRS_LAPIS_LAZULI = null;
    public static final Block SLAB_COAL = null;
    public static final Block SLAB_IRON = null;
    public static final Block SLAB_GOLD = null;
    public static final Block SLAB_REDSTONE = null;
    public static final Block SLAB_LAPIS_LAZULI = null;
    public static final Block FENCE_COAL = null;
    public static final Block FENCE_IRON = null;
    public static final Block FENCE_GOLD = null;
    public static final Block FENCE_REDSTONE = null;
    public static final Block FENCE_LAPIS_LAZULI = null;
    public static final Block FENCE_GATE_COAL = null;
    public static final Block FENCE_GATE_IRON = null;
    public static final Block FENCE_GATE_GOLD = null;
    public static final Block FENCE_GATE_REDSTONE = null;
    public static final Block FENCE_GATE_LAPIS_LAZULI = null;



    public static Block.Properties logProperties()
    {
        return Block.Properties.create(Material.WOOD).hardnessAndResistance(2.0F, 3.0F).harvestTool(ToolType.AXE).sound(SoundType.WOOD);
    }

    public static Block.Properties plankProperties()
    {
        return Block.Properties.create(Material.WOOD, MaterialColor.WOOD).hardnessAndResistance(2.0F, 3.0F).harvestTool(ToolType.AXE).sound(SoundType.WOOD);
    }

    public static Block.Properties leavesProperties()
    {
        return Block.Properties.create(Material.LEAVES).hardnessAndResistance(0.2F).tickRandomly().sound(SoundType.PLANT).notSolid();
    }

    public static Block.Properties saplingProperties()
    {
        return AbstractBlock.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().zeroHardnessAndResistance().sound(SoundType.PLANT);
    }

    public static Block.Properties coalProperties(Block.Properties blockProperties)
    {
        return blockProperties.harvestLevel(0);
    }

    public static Block.Properties ironProperties(Block.Properties blockProperties)
    {
        return blockProperties.harvestLevel(1);
    }

    public static Block.Properties goldProperties(Block.Properties blockProperties)
    {
        return blockProperties.harvestLevel(2);
    }

    public static Block.Properties redstoneProperties(Block.Properties blockProperties)
    {
        return blockProperties.harvestLevel(2);
    }

    public static Block.Properties lapisLazuliProperties(Block.Properties blockProperties)
    {
        return blockProperties.harvestLevel(2);
    }
}
