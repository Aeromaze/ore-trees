package com.aeromaze.oretrees.block;

import net.minecraft.block.BlockState;
import net.minecraft.block.StairsBlock;

import java.util.function.Supplier;

public class BlockOreStairs extends StairsBlock
{

    public BlockOreStairs(Supplier<BlockState> blockState, Properties properties)
    {
        super(blockState, properties);
    }
}
