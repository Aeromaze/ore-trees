package com.aeromaze.oretrees;

import com.aeromaze.oretrees.reference.Reference;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.stream.Collectors;

@Mod(Reference.MOD_ID)
public class OreTrees
{
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

}