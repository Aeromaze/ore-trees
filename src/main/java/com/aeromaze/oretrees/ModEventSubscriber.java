package com.aeromaze.oretrees;

import com.aeromaze.oretrees.block.*;
import com.aeromaze.oretrees.block.trees.CoalTree;
import com.aeromaze.oretrees.init.ModBlocks;
import com.aeromaze.oretrees.init.ModItems;
import com.aeromaze.oretrees.reference.Reference;
import net.minecraft.block.*;
import net.minecraft.block.trees.BirchTree;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistryEntry;

import java.util.function.Supplier;

@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModEventSubscriber
{
    @SubscribeEvent
    public static void onRegisterItems(RegistryEvent.Register<Item> event)
    {
        event.getRegistry().registerAll(
                // Items

                // BlockItems
                setup(new BlockItem(ModBlocks.LOG_COAL, ModItems.itemProperties()), "log_coal"),
                setup(new BlockItem(ModBlocks.LOG_IRON, ModItems.itemProperties()), "log_iron"),
                setup(new BlockItem(ModBlocks.LOG_GOLD, ModItems.itemProperties()), "log_gold"),
                setup(new BlockItem(ModBlocks.LOG_REDSTONE, ModItems.itemProperties()), "log_redstone"),
                setup(new BlockItem(ModBlocks.LOG_LAPIS_LAZULI, ModItems.itemProperties()), "log_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.PLANK_COAL, ModItems.itemProperties()), "plank_coal"),
                setup(new BlockItem(ModBlocks.PLANK_IRON, ModItems.itemProperties()), "plank_iron"),
                setup(new BlockItem(ModBlocks.PLANK_GOLD, ModItems.itemProperties()), "plank_gold"),
                setup(new BlockItem(ModBlocks.PLANK_REDSTONE, ModItems.itemProperties()), "plank_redstone"),
                setup(new BlockItem(ModBlocks.PLANK_LAPIS_LAZULI, ModItems.itemProperties()), "plank_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.LEAVES_COAL, ModItems.itemProperties()), "leaves_coal"),
                setup(new BlockItem(ModBlocks.LEAVES_IRON, ModItems.itemProperties()), "leaves_iron"),
                setup(new BlockItem(ModBlocks.LEAVES_GOLD, ModItems.itemProperties()), "leaves_gold"),
                setup(new BlockItem(ModBlocks.LEAVES_REDSTONE, ModItems.itemProperties()), "leaves_redstone"),
                setup(new BlockItem(ModBlocks.LEAVES_LAPIS_LAZULI, ModItems.itemProperties()), "leaves_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.SAPLING_COAL, ModItems.itemProperties()), "sapling_coal"),
                setup(new BlockItem(ModBlocks.SAPLING_IRON, ModItems.itemProperties()), "sapling_iron"),
                setup(new BlockItem(ModBlocks.SAPLING_GOLD, ModItems.itemProperties()), "sapling_gold"),
                setup(new BlockItem(ModBlocks.SAPLING_REDSTONE, ModItems.itemProperties()), "sapling_redstone"),
                setup(new BlockItem(ModBlocks.SAPLING_LAPIS_LAZULI, ModItems.itemProperties()), "sapling_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.WOOD_COAL, ModItems.itemProperties()), "wood_coal"),
                setup(new BlockItem(ModBlocks.WOOD_IRON, ModItems.itemProperties()), "wood_iron"),
                setup(new BlockItem(ModBlocks.WOOD_GOLD, ModItems.itemProperties()), "wood_gold"),
                setup(new BlockItem(ModBlocks.WOOD_REDSTONE, ModItems.itemProperties()), "wood_redstone"),
                setup(new BlockItem(ModBlocks.WOOD_LAPIS_LAZULI, ModItems.itemProperties()), "wood_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.STRIPPED_WOOD_COAL, ModItems.itemProperties()), "stripped_wood_coal"),
                setup(new BlockItem(ModBlocks.STRIPPED_WOOD_IRON, ModItems.itemProperties()), "stripped_wood_iron"),
                setup(new BlockItem(ModBlocks.STRIPPED_WOOD_GOLD, ModItems.itemProperties()), "stripped_wood_gold"),
                setup(new BlockItem(ModBlocks.STRIPPED_WOOD_REDSTONE, ModItems.itemProperties()), "stripped_wood_redstone"),
                setup(new BlockItem(ModBlocks.STRIPPED_WOOD_LAPIS_LAZULI, ModItems.itemProperties()), "stripped_wood_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.STRIPPED_LOG_COAL, ModItems.itemProperties()), "stripped_log_coal"),
                setup(new BlockItem(ModBlocks.STRIPPED_LOG_IRON, ModItems.itemProperties()), "stripped_log_iron"),
                setup(new BlockItem(ModBlocks.STRIPPED_LOG_GOLD, ModItems.itemProperties()), "stripped_log_gold"),
                setup(new BlockItem(ModBlocks.STRIPPED_LOG_REDSTONE, ModItems.itemProperties()), "stripped_log_redstone"),
                setup(new BlockItem(ModBlocks.STRIPPED_LOG_LAPIS_LAZULI, ModItems.itemProperties()), "stripped_log_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.STAIRS_COAL, ModItems.itemProperties()), "stairs_coal"),
                setup(new BlockItem(ModBlocks.STAIRS_IRON, ModItems.itemProperties()), "stairs_iron"),
                setup(new BlockItem(ModBlocks.STAIRS_GOLD, ModItems.itemProperties()), "stairs_gold"),
                setup(new BlockItem(ModBlocks.STAIRS_REDSTONE, ModItems.itemProperties()), "stairs_redstone"),
                setup(new BlockItem(ModBlocks.STAIRS_LAPIS_LAZULI, ModItems.itemProperties()), "stairs_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.SLAB_COAL, ModItems.itemProperties()), "slab_coal"),
                setup(new BlockItem(ModBlocks.SLAB_IRON, ModItems.itemProperties()), "slab_iron"),
                setup(new BlockItem(ModBlocks.SLAB_GOLD, ModItems.itemProperties()), "slab_gold"),
                setup(new BlockItem(ModBlocks.SLAB_REDSTONE, ModItems.itemProperties()), "slab_redstone"),
                setup(new BlockItem(ModBlocks.SLAB_LAPIS_LAZULI, ModItems.itemProperties()), "slab_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.FENCE_COAL, ModItems.itemProperties()), "fence_coal"),
                setup(new BlockItem(ModBlocks.FENCE_IRON, ModItems.itemProperties()), "fence_iron"),
                setup(new BlockItem(ModBlocks.FENCE_GOLD, ModItems.itemProperties()), "fence_gold"),
                setup(new BlockItem(ModBlocks.FENCE_REDSTONE, ModItems.itemProperties()), "fence_redstone"),
                setup(new BlockItem(ModBlocks.FENCE_LAPIS_LAZULI, ModItems.itemProperties()), "fence_lapis_lazuli"),

                setup(new BlockItem(ModBlocks.FENCE_GATE_COAL, ModItems.itemProperties()), "fence_gate_coal"),
                setup(new BlockItem(ModBlocks.FENCE_GATE_IRON, ModItems.itemProperties()), "fence_gate_iron"),
                setup(new BlockItem(ModBlocks.FENCE_GATE_GOLD, ModItems.itemProperties()), "fence_gate_gold"),
                setup(new BlockItem(ModBlocks.FENCE_GATE_REDSTONE, ModItems.itemProperties()), "fence_gate_redstone"),
                setup(new BlockItem(ModBlocks.FENCE_GATE_LAPIS_LAZULI, ModItems.itemProperties()), "fence_gate_lapis_lazuli")
        );
    }

    @SubscribeEvent
    public static void onRegisterBlocks(RegistryEvent.Register<Block> event)
    {
        Supplier<BlockState> getPlankCoalDefaultState = () -> ModBlocks.PLANK_COAL.getDefaultState();
        Supplier<BlockState> getPlankIronDefaultState = () -> ModBlocks.PLANK_IRON.getDefaultState();
        Supplier<BlockState> getPlankGoldDefaultState = () -> ModBlocks.PLANK_GOLD.getDefaultState();
        Supplier<BlockState> getPlankRedstoneDefaultState = () -> ModBlocks.PLANK_REDSTONE.getDefaultState();
        Supplier<BlockState> getPlankLapisLazuliDefaultState = () -> ModBlocks.PLANK_LAPIS_LAZULI.getDefaultState();


        event.getRegistry().registerAll(

                setup(new BlockOreLog(ModBlocks.coalProperties(ModBlocks.logProperties())
                        ), "log_coal"),
                setup(new BlockOreLog(ModBlocks.ironProperties(ModBlocks.logProperties())
                        ), "log_iron"),
                setup(new BlockOreLog(ModBlocks.goldProperties(ModBlocks.logProperties())
                        ), "log_gold"),
                setup(new BlockOreLog(ModBlocks.redstoneProperties(ModBlocks.logProperties())
                        ), "log_redstone"),
                setup(new BlockOreLog(ModBlocks.lapisLazuliProperties(ModBlocks.logProperties())
                        ), "log_lapis_lazuli"),

                setup(new BlockOrePlank(ModBlocks.coalProperties(ModBlocks.plankProperties())
                        ), "plank_coal"),
                setup(new BlockOrePlank(ModBlocks.ironProperties(ModBlocks.plankProperties())
                        ), "plank_iron"),
                setup(new BlockOrePlank(ModBlocks.goldProperties(ModBlocks.plankProperties())
                        ), "plank_gold"),
                setup(new BlockOrePlank(ModBlocks.redstoneProperties(ModBlocks.plankProperties())
                        ), "plank_redstone"),
                setup(new BlockOrePlank(ModBlocks.lapisLazuliProperties(ModBlocks.plankProperties())
                        ), "plank_lapis_lazuli"),

                setup(new BlockOreLeaves(ModBlocks.leavesProperties()
                        ), "leaves_coal"),
                setup(new BlockOreLeaves(ModBlocks.leavesProperties()
                        ), "leaves_iron"),
                setup(new BlockOreLeaves(ModBlocks.leavesProperties()
                        ), "leaves_gold"),
                setup(new BlockOreLeaves(ModBlocks.leavesProperties()
                        ), "leaves_redstone"),
                setup(new BlockOreLeaves(ModBlocks.leavesProperties()
                        ), "leaves_lapis_lazuli"),

                setup(new BlockOreSapling(new CoalTree(), ModBlocks.saplingProperties()
                        ), "sapling_coal"),
                setup(new BlockOreSapling(new BirchTree(), ModBlocks.saplingProperties()
                        ), "sapling_iron"),
                setup(new BlockOreSapling(new BirchTree(), ModBlocks.saplingProperties()
                        ), "sapling_gold"),
                setup(new BlockOreSapling(new BirchTree(), ModBlocks.saplingProperties()
                        ), "sapling_redstone"),
                setup(new BlockOreSapling(new BirchTree(), ModBlocks.saplingProperties()
                        ), "sapling_lapis_lazuli"),

                setup(new BlockOreRotatedPillarBlock(ModBlocks.coalProperties(ModBlocks.logProperties())
                        ), "wood_coal"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.ironProperties(ModBlocks.logProperties())
                        ), "wood_iron"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.goldProperties(ModBlocks.logProperties())
                        ), "wood_gold"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.redstoneProperties(ModBlocks.logProperties())
                        ), "wood_redstone"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.lapisLazuliProperties(ModBlocks.logProperties())
                        ), "wood_lapis_lazuli"),

                setup(new BlockOreRotatedPillarBlock(ModBlocks.coalProperties(ModBlocks.logProperties())
                        ), "stripped_wood_coal"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.ironProperties(ModBlocks.logProperties())
                        ), "stripped_wood_iron"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.goldProperties(ModBlocks.logProperties())
                        ), "stripped_wood_gold"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.redstoneProperties(ModBlocks.logProperties())
                        ), "stripped_wood_redstone"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.lapisLazuliProperties(ModBlocks.logProperties())
                        ), "stripped_wood_lapis_lazuli"),

                setup(new BlockOreRotatedPillarBlock(ModBlocks.coalProperties(ModBlocks.logProperties())
                        ), "stripped_log_coal"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.ironProperties(ModBlocks.logProperties())
                        ), "stripped_log_iron"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.goldProperties(ModBlocks.logProperties())
                        ), "stripped_log_gold"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.redstoneProperties(ModBlocks.logProperties())
                        ), "stripped_log_redstone"),
                setup(new BlockOreRotatedPillarBlock(ModBlocks.lapisLazuliProperties(ModBlocks.logProperties())
                        ), "stripped_log_lapis_lazuli"),

                setup(new BlockOreStairs(getPlankCoalDefaultState, ModBlocks.plankProperties()
                        ), "stairs_coal"),
                setup(new BlockOreStairs(getPlankIronDefaultState, ModBlocks.plankProperties()
                        ), "stairs_iron"),
                setup(new BlockOreStairs(getPlankGoldDefaultState, ModBlocks.plankProperties()
                        ), "stairs_gold"),
                setup(new BlockOreStairs(getPlankRedstoneDefaultState, ModBlocks.plankProperties()
                        ), "stairs_redstone"),
                setup(new BlockOreStairs(getPlankLapisLazuliDefaultState, ModBlocks.plankProperties()
                        ), "stairs_lapis_lazuli"),

                setup(new SlabBlock(ModBlocks.coalProperties(ModBlocks.plankProperties())
                        ), "slab_coal"),
                setup(new SlabBlock(ModBlocks.ironProperties(ModBlocks.plankProperties())
                        ), "slab_iron"),
                setup(new SlabBlock(ModBlocks.goldProperties(ModBlocks.plankProperties())
                        ), "slab_gold"),
                setup(new SlabBlock(ModBlocks.redstoneProperties(ModBlocks.plankProperties())
                        ), "slab_redstone"),
                setup(new SlabBlock(ModBlocks.lapisLazuliProperties(ModBlocks.plankProperties())
                        ), "slab_lapis_lazuli"),

                setup(new BlockOreFence(ModBlocks.coalProperties(ModBlocks.plankProperties())
                        ), "fence_coal"),
                setup(new BlockOreFence(ModBlocks.ironProperties(ModBlocks.plankProperties())
                        ), "fence_iron"),
                setup(new BlockOreFence(ModBlocks.goldProperties(ModBlocks.plankProperties())
                        ), "fence_gold"),
                setup(new BlockOreFence(ModBlocks.redstoneProperties(ModBlocks.plankProperties())
                        ), "fence_redstone"),
                setup(new BlockOreFence(ModBlocks.lapisLazuliProperties(ModBlocks.plankProperties())
                        ), "fence_lapis_lazuli"),

                setup(new BlockOreFenceGate(ModBlocks.coalProperties(ModBlocks.plankProperties())
                        ), "fence_gate_coal"),
                setup(new BlockOreFenceGate(ModBlocks.ironProperties(ModBlocks.plankProperties())
                        ), "fence_gate_iron"),
                setup(new BlockOreFenceGate(ModBlocks.goldProperties(ModBlocks.plankProperties())
                        ), "fence_gate_gold"),
                setup(new BlockOreFenceGate(ModBlocks.redstoneProperties(ModBlocks.plankProperties())
                        ), "fence_gate_redstone"),
                setup(new BlockOreFenceGate(ModBlocks.lapisLazuliProperties(ModBlocks.plankProperties())
                        ), "fence_gate_lapis_lazuli")
        );
    }
/*
    private static BlockOreLog createLogBlock(MaterialColor topColor, MaterialColor barkColor) {
        return new BlockOreLog(AbstractBlock.Properties.create(Material.WOOD, (state) -> {
            return state.get(BlockOreLog.AXIS) == Direction.Axis.Y ? topColor : barkColor;
        }).hardnessAndResistance(2.0F).sound(SoundType.WOOD));
    }*/

    //Sets the proper registry name for 'Item', 'Block', 'TileEntity', 'EntityEntry' and 'Dimension' entries.
    public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final String name)
    {
        return setup(entry, new ResourceLocation(Reference.MOD_ID, name));
    }

    public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final ResourceLocation registryName)
    {
        entry.setRegistryName(registryName);
        return entry;
    }
}
