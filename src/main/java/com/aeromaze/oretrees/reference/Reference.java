package com.aeromaze.oretrees.reference;

public class Reference
{
    public static final String MOD_ID = "oretrees";
    public static final String MOD_NAME = "Ore Trees";
    public static final String VERSION = "1.15.2-0.1.0";
}
