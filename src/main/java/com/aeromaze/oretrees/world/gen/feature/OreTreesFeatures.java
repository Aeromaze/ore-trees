package com.aeromaze.oretrees.world.gen.feature;

import com.aeromaze.oretrees.init.ModBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.gen.foliageplacer.AcaciaFoliagePlacer;
import net.minecraft.world.gen.trunkplacer.ForkyTrunkPlacer;

public class OreTreesFeatures
{
    public static final ConfiguredFeature<BaseTreeFeatureConfig, ?> COAL;

    private static <FC extends IFeatureConfig> ConfiguredFeature<FC, ?> register(String p_243968_0_, ConfiguredFeature<FC, ?> p_243968_1_) {
        return (ConfiguredFeature) Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, p_243968_0_, p_243968_1_);
    }


    static
    {
        COAL = register("coal",Feature.TREE.withConfiguration((new net.minecraft.world.gen.feature.BaseTreeFeatureConfig.Builder(new SimpleBlockStateProvider(States.LOG_COAL), new SimpleBlockStateProvider(States.LEAVES_COAL), new AcaciaFoliagePlacer(FeatureSpread.func_242252_a(2), FeatureSpread.func_242252_a(0)), new ForkyTrunkPlacer(5, 2, 2), new TwoLayerFeature(1, 0, 2))).setIgnoreVines().build()));
    }

    public static final class States
    {
        protected static final BlockState LOG_COAL;
        protected static final BlockState LEAVES_COAL;

        static
        {
            LOG_COAL = ModBlocks.LOG_COAL.getDefaultState();
            LEAVES_COAL = ModBlocks.LEAVES_COAL.getDefaultState();
        }
    }
}
